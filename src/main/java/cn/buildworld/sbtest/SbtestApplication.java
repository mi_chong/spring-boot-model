package cn.buildworld.sbtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication

/**
 * 扫描mapper包路径
 */
@MapperScan(basePackages = "cn.buildworld.sbtest.mapper")

/**
 * 扫描其它需要的包
 */
@ComponentScan(basePackages = {"org.n3r.idworker","cn.buildworld.sbtest"})

/**
 * 开启定时任务
 */
@EnableScheduling

/**
 * 开启异步任务
 */
@EnableAsync
public class SbtestApplication {
	public static void main(String[] args) {
		SpringApplication.run(SbtestApplication.class, args);
	}
}
