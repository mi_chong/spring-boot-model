package cn.buildworld.sbtest.err;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author MiChong
 * @Email: 1564666023@qq.com
 * @Create 2018-03-26 16:51
 * @Version: V1.0
 */

@ControllerAdvice
public class MyExceptionHandler {

    private static final String ERROR_PAGE = "error/error";

    @ExceptionHandler(value = Exception.class)
    public Object errorHandler(HttpServletRequest request, HttpServletResponse response, Exception e){

        ModelAndView view = new ModelAndView();

        view.addObject("exception",e);
        view.addObject("url",request.getRequestURL());
        view.setViewName(ERROR_PAGE);
        return view;
    }

}
