package cn.buildworld.sbtest.configer;

import cn.buildworld.sbtest.interceptor.OneInterCeptor;
import cn.buildworld.sbtest.interceptor.TwoInterCeptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * configer，配置拦截器
 *
 * @Author MiChong
 * @Email: 1564666023@qq.com
 * @Create 2018-03-28 10:14
 * @Version: V1.0
 */

@Configuration
public class WebMvcConfiger extends WebMvcConfigurerAdapter {

    /**
     * 重写父类的方法，在此处对拦截器进行设置
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * 拦截器按照顺序执行,如果将one改成 * 则表示拦截所有的请求
         */
        registry.addInterceptor(new TwoInterCeptor()).addPathPatterns("/one/**")
                .addPathPatterns("/two/**");
        registry.addInterceptor(new OneInterCeptor()).addPathPatterns("/two/**");
        super.addInterceptors(registry);
    }
}
