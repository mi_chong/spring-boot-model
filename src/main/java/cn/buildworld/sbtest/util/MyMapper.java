

package cn.buildworld.sbtest.util;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;
import tk.mybatis.mapper.common.base.delete.DeleteByPrimaryKeyMapper;
import tk.mybatis.mapper.common.condition.SelectByConditionMapper;
import tk.mybatis.mapper.common.ids.DeleteByIdsMapper;
import tk.mybatis.mapper.common.ids.SelectByIdsMapper;

/**
 * 继承自己的MyMapper
 *
 */
public interface MyMapper<T> extends
        Mapper<T>,
        MySqlMapper<T>,
        SelectByIdsMapper<T>,
        SelectByConditionMapper<T>,
        DeleteByIdsMapper<T>,
        DeleteByPrimaryKeyMapper<T> {
    //TODO
    //FIXME 特别注意，该接口不能被扫描到，否则会出错
}
