package cn.buildworld.sbtest.web;

import cn.buildworld.sbtest.model.HseCustomer;
import cn.buildworld.sbtest.model.StudentInfo;
import cn.buildworld.sbtest.service.HseCustomerService;
import cn.buildworld.sbtest.service.StudentInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author MiChong
 * @Email: 1564666023@qq.com
 * @Create 2018-03-26 14:29
 * @Version: V1.0
 */

@RestController
public class IndexController {

    @GetMapping("index")
    public String index(){
        return "index";
    }

    @GetMapping("index2")
    public String index2(){
        return "index2";
    }

    @GetMapping("index3")
    public String index3(){
        int i = 1;

        System.out.println(1312312312);
        System.out.println(i / 0);

        return "index2";
    }

    @Autowired
    private HseCustomerService hseCustomerService;
    @GetMapping("list")
    public Object getList(@RequestParam(defaultValue = "0")Integer fromId,
                          @RequestParam(defaultValue = "2")Integer limit ){

        //初始化page插件，传入分页参数
        PageHelper.startPage(fromId,limit);
        List<HseCustomer> list = hseCustomerService.getList();

        //包装想要返回的结果，包含多种信息
        PageInfo pageInfo = new PageInfo(list);

        return pageInfo;
    }

    /**
     * redis测试
     */

    @Autowired
    //spring-boot自带的模板
    private StringRedisTemplate redisTemplate;
    @GetMapping("redis")
    public Object getRedis(){

        //往redis数据库存入数据
        redisTemplate.opsForValue().set("name","赵晓东");

        //读出redis中的数据
        return redisTemplate.opsForValue().get("name");

    }

}
