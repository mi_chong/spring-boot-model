package cn.buildworld.sbtest.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时任务
 *
 * @Author MiChong
 * @Email: 1564666023@qq.com
 * @Create 2018-03-28 9:02
 * @Version: V1.0
 */
@Component
public class TimeTask {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");

    /**
     * 从0开始，每隔三秒执行一次操作
     */
    @Scheduled(cron = "0/3 * * * * ? ")
    public void ShowTime(){
        System.out.println("现在时间："+DATE_FORMAT.format(new Date()));
    }


}
