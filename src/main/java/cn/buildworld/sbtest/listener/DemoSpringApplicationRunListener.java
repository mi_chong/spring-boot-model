package cn.buildworld.sbtest.listener;

import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @Author MiChong
 * @Email: 1564666023@qq.com
 * @Create 2018-03-26 10:33
 * @Version: V1.0
 */

public class DemoSpringApplicationRunListener implements SpringApplicationRunListener {
    @Override
    public void starting() {

        System.out.println("listener start!");
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment configurableEnvironment) {

    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext configurableApplicationContext) {

    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext configurableApplicationContext) {

    }

    @Override
    public void finished(ConfigurableApplicationContext configurableApplicationContext, Throwable throwable) {

    }
}
